import java.text.DecimalFormat;
import java.util.Scanner; //imports Scanner
import java.io.*;

public class Herman_PP2 {
    
    public static void main(String[] args) throws IOException {
        
        int employeeSelect;
        
        do {
            
        //employee selection menu:
        
        System.out.println("1. Enter customer information");
        System.out.println(" ");
        System.out.println("2. Price Lookup");
        System.out.println(" ");
        System.out.println("3. Display Total Bill");
        System.out.println(" ");
        System.out.println("4. Quit");
        System.out.println(" ");
        
        Scanner employeeInput = new Scanner(System.in); //Creates Scanner
        
        employeeSelect = employeeInput.nextInt(); //Employee is prompted to put in their selection number (1, 2, 3, or 4)
        
        
        //First Scenario:
        
        if (employeeSelect == 1) {
            
            FileWriter fwriter = new FileWriter("Herman_PP2.txt", true);
            PrintWriter outputFile = new PrintWriter(fwriter);
            
            
            
            
            System.out.println("Customer Name: ");
            String customerName;
            customerName = employeeInput.next();
            String customerLastName;
            customerLastName = employeeInput.next();
            outputFile.println(customerName + " " + customerLastName);
            
            
            System.out.println("Customer Address: ");
            
            String customerAddress;
            String customerAddress1;
            String customerAddress2;
            String customerAddress3;
            String customerAddress4;
            String customerAddress5;
            
            
            customerAddress = employeeInput.next();
            customerAddress1 = employeeInput.next();
            customerAddress2= employeeInput.next();
            customerAddress3 = employeeInput.next();
            customerAddress4 = employeeInput.next();
            customerAddress5 = employeeInput.next();
            
            outputFile.println(customerAddress + " " + customerAddress1 + " " + customerAddress2);
            outputFile.println(customerAddress3 + " " + customerAddress4 + " " + customerAddress5);
            
            System.out.println("Customer Email: ");
            String customerEmail;
            customerEmail = employeeInput.next();
            outputFile.println(customerEmail);
            
            
            outputFile.println("                "); // adds a space for next customer
            
            
            
            outputFile.close();
            
            
            
            
            System.out.println(customerName + " " + customerLastName);
            System.out.println(customerAddress + " " + customerAddress1 + " " + customerAddress2);
            System.out.println(customerAddress3 + " " + customerAddress4 + " " + customerAddress5);
            System.out.println(customerEmail);
            System.out.println(customerName + " has been added to our customer list."); //confirms that the customer has been added.
            
            
            
            
        }
        
        //Second Scenario:
        
        
        Scanner lookupScan = new Scanner(System.in);
        
        if (employeeSelect == 2) {
            
            
            DecimalFormat priceFormatter = new DecimalFormat("00.00");
            
            System.out.println(" ");
            System.out.println("1. Shoes");
            System.out.println("2. T-Shirts");
            System.out.println("3. Shorts");
            System.out.println("4. Caps");
            System.out.println("5. Jackets");
            System.out.println(" ");
            
            int lookupPrice;
            lookupPrice = lookupScan.nextInt();
            
            double shoePrice = 50.00;
            double shirtPrice = 30.00;
            double shortsPrice = 75.00;
            double capPrice = 15.00;
            double jacketPrice = 100.00;
            
            
            
            if (lookupPrice == 1) {
                
                System.out.println(" ");
                System.out.print("$" + priceFormatter.format(shoePrice));
                System.out.println(" ");
            }
            
            if (lookupPrice == 2) {
                
                System.out.println(" ");
                System.out.print("$" + priceFormatter.format(shirtPrice));
                System.out.println(" ");
            }
            
            if (lookupPrice == 3) {
                System.out.println(" ");
                System.out.print("$" + priceFormatter.format(shortsPrice));
                System.out.println(" ");
            }
            
            if (lookupPrice == 4) {
                System.out.println(" ");
                System.out.print("$" + priceFormatter.format(capPrice));
                System.out.println(" ");
            }
            
            
            if (lookupPrice == 5) {
                
                System.out.println(" ");
                System.out.print("$" + priceFormatter.format(jacketPrice));
                System.out.println(" ");
            }
            
            else if (lookupPrice >= 6){
                System.out.println(" ");
                System.out.println("Error.");
                System.out.println(" ");
            }
            
        }
        
        
        if (employeeSelect == 3) {
          
        	final double taxRate = 0.08; //tax is constant - so final is used
            
            String sayQuantity = "Quantity: ";
            System.out.println(sayQuantity); //Asks employee how many of product
            
            int custQuant;
            custQuant = employeeInput.nextInt();
            
            
            String ProductMenu ="Products: ";
            String blank = " ";
            String OptionShoes = "Shoes - $50.00";
            String OptionShirt = "Shirt - $30.00";
            String OptionShorts = "Shorts - $75.00";
            String OptionCaps = "Cap - $15.00";
            String OptionJacket = "Jacket - $100.00";
            
            
            
            String sayProduct = "Product: ";
            
            System.out.println(sayProduct); //Asks employee what product is
            
            System.out.println("1. Shoes");
            System.out.println("2. Shirts");
            System.out.println("3. Shorts");
            System.out.println("4. Cap");
            System.out.println("5. Jacket");
            
            int custProduct;
            custProduct = employeeInput.nextInt();
            
            double prodPrice;
            
            double shoeOption = 50.00;
            double shirtOption = 30.00;
            double shortsOption = 75.00;
            double capOption = 15.00;
            double jacketOption = 100.00;
            
            
            if (custProduct == 1){
            	
                prodPrice = shoeOption;
                
                double finTaxadd = prodPrice * taxRate;
                double taxFinal = finTaxadd * custQuant;
                double prodPrice2 = prodPrice * custQuant;
                double finalCost = prodPrice2 + taxFinal;
                
                //output
                
                
                String filename = "Herman_PP2.txt";
                
                File file = new File(filename);
                Scanner inputFile = new Scanner(file);
                
                while (inputFile.hasNext()) {
                    
                    String customerNames = inputFile.nextLine();
                    String customerAddress0 = inputFile.nextLine();
                    String customerAddress00 = inputFile.nextLine();
                    String customerEmails = inputFile.nextLine();
                    
                    System.out.println(" ");
                    
                    System.out.println(customerNames);
                    System.out.println(customerAddress0);
                    System.out.println(customerAddress00);
                    System.out.println(customerEmails);
                    
                }
                
                inputFile.close();
                
                System.out.println(" ");
                
                String prodPurchased = "Product Purchased";
                String Quantity = "Quantity";
                String totCost = "Total Cost";
                
                
                System.out.printf("%s %16s %17s \n", prodPurchased,Quantity, totCost);
                
                System.out.printf("%s %,28d %,19.2f \n", custProduct, custQuant, prodPrice2);
                
                System.out.printf("Tax (@ 8%%): %,40.2f \n", taxFinal);
                
                
                System.out.printf("%s %,41.2f", totCost,  finalCost);
                
                System.out.println("      ");
                System.out.println("      ");
                
                
            }
            
            if (custProduct == 2) {
            	prodPrice = shirtOption;
            	
                double finTaxadd = prodPrice * taxRate;
                double taxFinal = finTaxadd * custQuant;
                double prodPrice2 = prodPrice * custQuant;
                double finalCost = prodPrice2 + taxFinal;
                
                //output
                
                
                String filename = "Herman_PP2.txt";
                
                File file = new File(filename);
                Scanner inputFile = new Scanner(file);
                
                while (inputFile.hasNext()) {
                    
                    String customerNames = inputFile.nextLine();
                    String customerAddress0 = inputFile.nextLine();
                    String customerAddress00 = inputFile.nextLine();
                    String customerEmails = inputFile.nextLine();
                    
                    System.out.println(" ");
                    
                    System.out.println(customerNames);
                    System.out.println(customerAddress0);
                    System.out.println(customerAddress00);
                    System.out.println(customerEmails);
                    
                }
                
                inputFile.close();
                
                System.out.println(" ");
                
                String prodPurchased = "Product Purchased";
                String Quantity = "Quantity";
                String totCost = "Total Cost";
                
                
                System.out.printf("%s %16s %17s \n", prodPurchased,Quantity, totCost);
                
                System.out.printf("%s %,28d %,19.2f \n", custProduct, custQuant, prodPrice2);
                
                System.out.printf("Tax (@ 8%%): %,40.2f \n", taxFinal);
                
                
                System.out.printf("%s %,41.2f", totCost,  finalCost);
                
                System.out.println("      ");
                System.out.println("      ");
                
            }
            
            if (custProduct == 3){
            	prodPrice = shortsOption;
            	
                double finTaxadd = prodPrice * taxRate;
                double taxFinal = finTaxadd * custQuant;
                double prodPrice2 = prodPrice * custQuant;
                double finalCost = prodPrice2 + taxFinal;
                
                //output
                
                
                String filename = "Herman_PP2.txt";
                
                File file = new File(filename);
                Scanner inputFile = new Scanner(file);
                
                while (inputFile.hasNext()) {
                    
                    String customerNames = inputFile.nextLine();
                    String customerAddress0 = inputFile.nextLine();
                    String customerAddress00 = inputFile.nextLine();
                    String customerEmails = inputFile.nextLine();
                    
                    System.out.println(" ");
                    
                    System.out.println(customerNames);
                    System.out.println(customerAddress0);
                    System.out.println(customerAddress00);
                    System.out.println(customerEmails);
                    
                }
                
                inputFile.close();
                
                System.out.println(" ");
                
                String prodPurchased = "Product Purchased";
                String Quantity = "Quantity";
                String totCost = "Total Cost";
                
                
                System.out.printf("%s %16s %17s \n", prodPurchased,Quantity, totCost);
                
                System.out.printf("%s %,28d %,19.2f \n", custProduct, custQuant, prodPrice2);
                
                System.out.printf("Tax (@ 8%%): %,40.2f \n", taxFinal);
                
                
                System.out.printf("%s %,41.2f", totCost,  finalCost);
                
                System.out.println("      ");
                System.out.println("      ");
                
            }
            
            if (custProduct == 4){
            	prodPrice = capOption;
            	
                double finTaxadd = prodPrice * taxRate;
                double taxFinal = finTaxadd * custQuant;
                double prodPrice2 = prodPrice * custQuant;
                double finalCost = prodPrice2 + taxFinal;
                
                //output
                
                
                String filename = "Herman_PP2.txt";
                
                File file = new File(filename);
                Scanner inputFile = new Scanner(file);
                
                while (inputFile.hasNext()) {
                    
                    String customerNames = inputFile.nextLine();
                    String customerAddress0 = inputFile.nextLine();
                    String customerAddress00 = inputFile.nextLine();
                    String customerEmails = inputFile.nextLine();
                    
                    System.out.println(" ");
                    
                    System.out.println(customerNames);
                    System.out.println(customerAddress0);
                    System.out.println(customerAddress00);
                    System.out.println(customerEmails);
                    
                }
                
                inputFile.close();
                
                System.out.println(" ");
                
                String prodPurchased = "Product Purchased";
                String Quantity = "Quantity";
                String totCost = "Total Cost";
                
                
                System.out.printf("%s %16s %17s \n", prodPurchased,Quantity, totCost);
                
                System.out.printf("%s %,28d %,19.2f \n", custProduct, custQuant, prodPrice2);
                
                System.out.printf("Tax (@ 8%%): %,40.2f \n", taxFinal);
                
                
                System.out.printf("%s %,41.2f", totCost,  finalCost);
                
                System.out.println("      ");
                System.out.println("      ");
            }
            
            if (custProduct == 5) {
            	prodPrice = jacketOption;
            	
                double finTaxadd = prodPrice * taxRate;
                double taxFinal = finTaxadd * custQuant;
                double prodPrice2 = prodPrice * custQuant;
                double finalCost = prodPrice2 + taxFinal;
                
                //output
                
                
                String filename = "Herman_PP2.txt";
                
                File file = new File(filename);
                Scanner inputFile = new Scanner(file);
                
                while (inputFile.hasNext()) {
                    
                    String customerNames = inputFile.nextLine();
                    String customerAddress0 = inputFile.nextLine();
                    String customerAddress00 = inputFile.nextLine();
                    String customerEmails = inputFile.nextLine();
                    
                    System.out.println(" ");
                    
                    System.out.println(customerNames);
                    System.out.println(customerAddress0);
                    System.out.println(customerAddress00);
                    System.out.println(customerEmails);
                    
                }
                
                inputFile.close();
                
                System.out.println(" ");
                
                String prodPurchased = "Product Purchased";
                String Quantity = "Quantity";
                String totCost = "Total Cost";
                
                
                System.out.printf("%s %16s %17s \n", prodPurchased,Quantity, totCost);
                
                System.out.printf("%s %,28d %,19.2f \n", custProduct, custQuant, prodPrice2);
                
                System.out.printf("Tax (@ 8%%): %,40.2f \n", taxFinal);
                
                
                System.out.printf("%s %,41.2f", totCost,  finalCost);
                
                System.out.println("      ");
                System.out.println("      ");
            }
            
            
            
            
            
        }
        
        if (employeeSelect == 4) {
        	
        	System.exit(0);
        }
          
    }
        
        while (employeeSelect != 4);
        
      
    
    }
}


/* References :
 
 1.Starting out with Java: From Control Structures through Objects - 5th edition: Addison-Wesley
 2. http://www.reddit.com/r/java/comments/1gcz20/reddit_i_need_your_help_lines_101108_not_working/
 
 */
















