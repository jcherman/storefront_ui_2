STOREFRONT_UI_2
===============

Programming Fundamentals Homework 2

===============


Modify your program to do the following:
1.    From Previous programming project (If the sales associate selects option 1, the program should allow the sales associate to input their name, address, and e-mail address. Your program should then display this customer information to the screen with a message that the customer has been added to the customer list.) Modify your option 1 by adding the new customer information to an external file. (Hint*See Section 4.10 in the textbook)
2.    Modify your program and add a new menu option for price lookup. If the sales associate selects the option for price lookup the program should allow the sales associate to choose a product from the following options:
1.     Shoes
2.    T-shirts
3.    Shorts
4.    Caps
5.    Jackets
Once they have selected the type of product the cost of the item will be displayed to the screen.
3.    Modify your option 3, total bill so that the customer information is retrieved from the external file. Also, modify your program to retrieve the price of the product. 
Your program should now ask the sales associate for the following information: customers name, the product and the quantity. The price should be retrieved from your program. The customer’s email and address should be retrieved from the external file that you created in item 1 above.
4.    Modify your program to include a loop so that the user is continually asked the main menu until they have selected option number 4 to Quit the program.
 
Your option menu will now look like this:
1.     Enter customer information
2.     Price Lookup
3.     Display Total Bill
4.    Quit

Your total bill should now look like this:
 
John Doe
2206 Anywhere Street
Anywhere, TX. 88888
john.doe@anywhere.com
Product Purchased                  Quantity                      Total Cost
Shoes                                           2                               $100.00
Shorts                                          1                                $75.00
Total Cost:                                                                   $175.00

